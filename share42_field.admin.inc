<?php

/**
 *  Configuration form.
 *  @see share42_field_field_conf_validate()
 *  @see share42_field_field_conf_submit()
 */
function share42_field_field_conf($form, $form_state, $entity_type, $bundle_name) {
  $form = array();
  $default_values = share42_field_get_conf($entity_type, $bundle_name);
  $field_list = field_info_instances($entity_type, $bundle_name);
  $select = array();
  $select['textfield'] = array('' => t('None'));
  $select['image'] = array('' => t('None'));
  $select['textarea'] = array('' => t('None'));
  foreach($field_list as $field_name => $field_info) {
    if($field_info['widget']['type'] == 'text_textfield') {
      $select['textfield'][$field_name] = $field_info['label'];
    }
    if($field_info['widget']['type'] == 'image_image') {
      $select['image'][$field_name] = $field_info['label'];
    }
    if($field_info['widget']['type'] == 'text_textarea') {
      $select['textarea'][$field_name] = $field_info['label'];
    }
  }
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );
  $form['bundle_name'] = array(
    '#type' => 'value',
    '#value' =>  $bundle_name,
  );
  $form['options']['storage_title'] = array(
    '#type' => 'select',
    '#title' => t('Page title'),
    //'#description' => t(''),
    '#options' => $select['textfield'],
  );
  $form['options']['storage_image'] = array(
    '#type' => 'select',
    '#title' => t('Image'),
    //'#description' => t(''),
    '#options' => $select['image'],
  );
  $form['options']['storage_description'] = array(
    '#type' => 'select',
    '#title' => t('Description of a page'),
    //'#description' => t(''),
    '#options' => $select['textarea'],
  );
  $form['options']['top1'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance from the top of the page to the panel'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => 0,
    //'#description' => t(''),
  );
  $form['options']['top2'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance from the top of the visible area of the page to the panel'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => 0,
    //'#description' => t(''),
  );
  $form['options']['margin'] = array(
    '#type' => 'textfield',
    '#title' => t('Horizontal displacement of the panel'),
    '#size' => 6,
    '#maxlength' => 6,
    '#default_value' => 0,
    //'#description' => t(''),
  );

  $scripts = file_scan_directory(SHARE42_FIELD_JS_DIR, '/^share42\.js$/');
  if(!empty($scripts)) {
    $form['options']['js'] = array(
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => t('JavaScript'),
      '#default_value' => SHARE42_FIELD_JS_DIR . '/shre42.js',
      //'#description' => t(''),
    );
    foreach($scripts as $key => $script) {
      $form['options']['js']['#options'][$key] = $script->uri;
    }
  }
  else {
    $message = array(
      'type' => 'ul',
      'title' => '',
      'attributes' => array(),
    );
    $message['items'][] = t('Please visit !url and create a widget.', array('!url' => l('share42.com', 'http://share42.com')));
    $message['items'][] = t('Extract the received archive in the directory %dir.', array('%dir' => DRUPAL_ROOT . '/' . SHARE42_FIELD_JS_DIR));
    form_set_error('', theme_item_list($message));
    $form['submit']['#disabled'] = TRUE;
  }
  foreach($form['options'] as $option => $values) {
    if(isset($default_values[$option])) {
      $form['options'][$option]['#default_value'] = $default_values[$option];
    }
  }
  $form['options']['#type'] = 'container';
  $form['options']['#tree'] = TRUE;

  $form['submit']['#type'] = 'submit';
  $form['submit']['#name'] = 'save';
  $form['submit']['#value'] = t('Save');

  return $form;
}

/**
 *  Validates the configuration form.
 *  @see share42_field_field_conf()
 *  @see share42_field_field_conf_submit()
 */
function share42_field_field_conf_validate($form, $form_state) {
  _share42_field_validate_integer('top1', $form['options']['top1'], 0, 65535);
  _share42_field_validate_integer('top2', $form['options']['top2'], 0, 65535);
  _share42_field_validate_integer('margin', $form['options']['margin'], -32768, 32767);
}

/**
 *  Validates the numeric field.
 */
function _share42_field_validate_integer($field, $element, $min_val, $max_val) {
  $value = $element['#value'];
  if(!is_numeric($value) || intval($value) != $value) {
    form_set_error($field, t('%name must be an integer.', array('%name' => $element['#title'])));
  }
  elseif ($value < $min_val) {
    form_set_error($field, t('%name must be an greater %min.', array('%name' => $element['#title'], '%min' => $min_val)));
  }
  elseif ($value > $max_val) {
    form_set_error($field, t('%name must be an lesser %max.', array('%name' => $element['#title'], '%max' => $max_val)));
  }
}

/**
 *  Submitted the configuration form.
 *  @see share42_field_field_conf()
 *  @see share42_field_field_conf_validate()
 */
function share42_field_field_conf_submit($form, $form_state) {
  if($form_state['submitted']) {
    share42_field_set_conf($form_state['values']['entity_type'], $form_state['values']['bundle_name'], $form_state['values']['options']);
  }
}
